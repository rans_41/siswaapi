<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $api_key = function($request, $response, $next) {

        $error = null;

        if($request->getHeader('api_key')) {

            $key = $request->getHeader('api_key')[0];

            try {
                $sql = "SELECT * FROM user where api_key = :api_key";
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam("api_key", $key);
                $stmt->execute();
                $result = $stmt->fetchObject();    

                if(!$result) {
                    $error = 'Key tidak dapat ditemukan';
                }

            } catch(PDOException $e) {
                $error = array('error'=>$e->getMessage());
            }
        } else {
            $error = 'Masukkan Key';
        }

        if($error) {
            return $response->withJson(array('error'=>$error), 401);
        }

        return $next($request, $response);

    };


    $app->post('/register', function (Request $request, Response $response, array $args) {

        $input = $request->getParsedBody();

        $id = trim(strip_tags($input['id']));
        $nama = trim(strip_tags($input['nama']));
        $email = trim(strip_tags($input['email']));
        $password = trim(strip_tags($input['password']));
        $api_key = trim(strip_tags($input['api_key']));

        $sql = "INSERT INTO user(id, nama, email, password, api_key) VALUES (:id, :nama, :email, :password, :api_key)";
        $sth = $this->db->prepare($sql);

        $sth->bindParam("id", $id);
        $sth->bindParam("nama", $nama);                       
        $sth->bindParam("email", $email);                  
        $sth->bindParam("password", $password); 
        $sth->bindParam("api_key", $api_key);

        $post = $sth->execute();

        if($post) {
            return $this->response->withJson(['status' => 'success','data' => 'berhasil menambahkan user'], 200); 
        } else {
            return $this->response->withJson(['status' => 'error','data' => 'gagal menambahkan user'], 200); 
        }

    });

    $app->post('/login', function (Request $request, Response $response, array $args) {

        $input = $request->getParsedBody();

        $email = trim(strip_tags($input['email']));
        $password = trim(strip_tags($input['password']));

        $sql = "SELECT id, email, api_key  FROM `user` WHERE email=:email AND `password`=:password";
        $sth = $this->db->prepare($sql);

        $sth->bindParam("email", $email);
        $sth->bindParam("password", $password);

        $sth->execute();

        $user = $sth->fetchObject();       

        if(!$user) {
            return $this->response->withJson(['status' => 'error', 'message' => 'gagal login']);  
        }

        return $this->response->withJson(['status' => 'success','data' => $user]); 

    });


    $app->group('/api', function(\Slim\App $app) {

        $app->get("/siswa", function (Request $request, Response $response) {
            $sql = "SELECT * FROM data_siswa";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $response->withJson(["status" => "success", "data" => $result], 200);
        });

        $app->get("/siswa/{id_siswa}", function (Request $request, Response $response, array $args) {
            $id_siswa = $args["id_siswa"];
            $sql = "SELECT * FROM data_siswa WHERE id_siswa=:id_siswa";
            $stmt = $this->db->prepare($sql);
            $stmt->execute([":id_siswa" => $id_siswa]);
            $result = $stmt->fetch();
            return $response->withJson(["status" => "success", "data" => $result], 200);
        });

        $app->post("/siswa", function (Request $request, Response $response) {

            $input = $request->getParsedBody();

            $sql = "INSERT INTO data_siswa (id_siswa, nama, alamat, jurusan, kelamin) VALUE (:id_siswa, :nama, :alamat, :jurusan, :kelamin)";
            $sth = $this->db->prepare($sql);

            $data = [
                ":id_siswa" => $input["id_siswa"],
                ":nama" => $input["nama"],
                ":alamat" => $input["alamat"],
                ":jurusan" => $input["jurusan"],
                ":kelamin" => $input["kelamin"],
            ];

            if($sth->execute($data))
                return $response->withJson(["status" => "success","data" => "berhasil menambahkan data siswa"], 200);

            return $response->withJson(["status" => "error","data" => "gagal menambahkan data siswa"], 200);
        });
    
        $app->put('/siswa/{id_siswa}', function (Request $request, Response $response, $args) {

            $id_siswa = $args["id_siswa"];

            $input = $request->getParsedBody();

            $sql = "UPDATE data_siswa SET nama=:nama, alamat=:alamat, jurusan=:jurusan, kelamin=:kelamin WHERE id_siswa=:id_siswa";
            $sth = $this->db->prepare($sql);

            $data = [
                ":id_siswa" => $id_siswa,
                ":nama" => $input["nama"],
                ":alamat" => $input["alamat"],
                ":jurusan" => $input["jurusan"],
                ":kelamin" => $input["kelamin"]
            ];

            if($sth->execute($data))
                return $response->withJson(["status" => "success","data"=>"berhasil mengubah data siswa"], 200); 

            return $response->withJson(["status" => "error","data"=>"gagal mengubah data siswa"], 200); 
        });
    
        $app->delete('/siswa/{id_siswa}', function (Request $request, Response $response, $args) {

            $id_siswa = $args["id_siswa"];

            $sql = "DELETE FROM data_siswa WHERE id_siswa=:id_siswa";
            $sth = $this->db->prepare($sql);

            $data = [
                ":id_siswa" => $id_siswa
            ];

            if($sth->execute($data))
                return $response->withJson(['status' => "success","data"=>"berhasil menghapus data siswa"], 200); 

            return $response->withJson(['status' => "error","data"=>"gagal menghapus data siswa"], 200); 
        });

    })->add($api_key);
};